
window.Vue = require('vue');
import VeeValidate from 'vee-validate';
import wysiwyg from "vue-wysiwyg";

const files = require.context('./', true, /\.vue$/i);
files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.use(VeeValidate);
Vue.use(wysiwyg, {}); // config is optional. more below

const app = new Vue({
    el: '#app',
    data() {
        return {
            menuIsOpen: false
        }
    },
    methods: {
        toggleMenu() {
            this.menuIsOpen = !this.menuIsOpen;
        }
    }
});