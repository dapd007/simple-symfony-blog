<?php


namespace App\Controller;

use App\Entity\Image;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class UploadsController extends AbstractController
{
    public function handleUpload(Request $request) {
        $output = [];

        $file = $request->files->get('file');

        // generate a new filename (safer, better approach)
        $fileName = md5(uniqid()).'.'.$file->guessExtension();
        $originalFileName = $file->getClientOriginalName();

        // set your uploads directory
        $uploadDir = __DIR__ . '/../../public/uploads/';
        if (!file_exists($uploadDir) && !is_dir($uploadDir)) {
            mkdir($uploadDir, 0775, true);
        }
        if ($file->move($uploadDir, $fileName)) {

            $entityManager = $this->getDoctrine()->getManager();

            $image = new Image();
            $image->setFileName($originalFileName);
            $image->setPath($fileName);
            $image->setUploadedAt(new \DateTime());

            $entityManager->persist($image);
            $entityManager->flush();

            $output['id'] = $image->getId();
            $output['path'] = $image->getPath();

        }
        return new JsonResponse($output);
    }
}