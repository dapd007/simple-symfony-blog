<?php


namespace App\Controller;

use App\Entity\Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

class ContactController extends AbstractController
{
    public function showForm() {
        return $this->render('contact.html.twig');
    }

    public function saveMessage() {
        $request = Request::createFromGlobals();

        $entityManager = $this->getDoctrine()->getManager();

        $message = new Message();
        $message->setName($request->request->get('name'));
        $message->setEmail($request->request->get('email'));
        $message->setMessage($request->request->get('message'));
        $message->setCreatedAt(new \DateTime());

        // tell Doctrine you want to (eventually) save the Product (no queries yet)
        $entityManager->persist($message);

        // actually executes the queries (i.e. the INSERT query)
        $entityManager->flush();

        $this->addFlash(
            'success',
            'Hola ' . $message->getName() . ', tu mensaje ha sido enviado. Nos pondremos en contacto lo más pronto posible.'
        );

        return $this->redirectToRoute('contact');
    }
}