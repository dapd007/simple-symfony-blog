<?php


namespace App\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class HomeController extends AbstractController
{
    public function home() {
        $repository = $this->getDoctrine()->getRepository(Post::class);
        $posts = $repository->getRecent(6);

        return $this->render('home.html.twig', ['recent' => $posts]);
    }
}