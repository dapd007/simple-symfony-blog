<?php


namespace App\Controller;

use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BlogController extends AbstractController
{
    public function list() {
        $repository = $this->getDoctrine()->getRepository(Post::class);
        $posts = $repository->findAllSorted();

        return $this->render('blog/blog_list.html.twig', ['posts' => $posts]);
    }

    public function show(Post $post) {
        $repository = $this->getDoctrine()->getRepository(Post::class);
        $posts = $repository->findAllExcept($post->getId());

        return $this->render('blog/blog_detail.html.twig', ['post' => $post, 'recent' => $posts]);
    }
}