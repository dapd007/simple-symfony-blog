<?php


namespace App\Controller;

use App\Entity\Image;
use App\Entity\Message;
use App\Entity\Post;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class AdminController extends AbstractController
{

    /**
     * Admin section index.
     *
     * Redirects to Posts section
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function index() {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        return $this->redirectToRoute('posts');
    }

    /**
     * Admin messages list
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function messagesList() {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $repository = $this->getDoctrine()->getRepository(Message::class);
        $messages = $repository->findAll();

        return $this->render('admin/messages_list.html.twig', ['messages' => $messages]);
    }
}