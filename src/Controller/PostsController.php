<?php


namespace App\Controller;

use App\Entity\Image;
use App\Entity\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class PostsController extends AbstractController
{
    /**
     * Admin posts list
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postsList() {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $repository = $this->getDoctrine()->getRepository(Post::class);
        $posts = $repository->findAllSorted();

        return $this->render('admin/posts_list.html.twig', [
            'posts' => $posts
        ]);
    }

    /**
     * Admin create post view
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postsCreate() {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('admin/posts_form.html.twig');
    }

    /**
     * Store new post in database
     *
     * @param  Security  $security
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     * @throws \Exception
     */
    public function postsStore(Security $security) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $request = Request::createFromGlobals();
        $user = $security->getUser();

        $imageRepository = $this->getDoctrine()->getRepository(Image::class);
        $entityManager = $this->getDoctrine()->getManager();

        $post = new Post();
        $post->setTitle($request->request->get('title'));
        $post->setContent($request->request->get('content'));
        $post->setAuthor($user);
        $post->setCreatedAt(new \DateTime());

        $imageId = $request->request->get('image_id');

        if ($imageId)
            $post->setImage($imageRepository->findOneBy(['id' => $imageId]));

        $entityManager->persist($post);
        $entityManager->flush();

        return $this->redirectToRoute('posts');
    }

    /**
     * Render post form
     *
     * @param  Post  $post
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postsEdit(Post $post) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        return $this->render('admin/posts_form.html.twig', ['post' => $post]);
    }

    /**
     * Update post by id
     *
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function postsUpdate($id) {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $request = Request::createFromGlobals();

        $imageRepository = $this->getDoctrine()->getRepository(Image::class);
        $entityManager = $this->getDoctrine()->getManager();
        $post = $entityManager->getRepository(Post::class)->find($id);

        $post->setTitle($request->request->get('title'));
        $post->setContent($request->request->get('content'));

        $imageId = $request->request->get('image_id');

        if ($imageId)
            $post->setImage($imageRepository->findOneBy(['id' => $imageId]));
        else
            $post->setImage(null);

        $entityManager->flush();
        return $this->redirectToRoute('posts');
    }
}